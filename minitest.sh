"""
Copyright [2019] [NETWORK•ID]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
#copyright gw doain gk halal dan gw gk ihklas
#contact:hendrikpurwanto271@gmail.com
clear
blue='\033[34;1m'
green='\033[32;1m'
purple='\033[35;1m'
cyan='\033[36;1m'
red='\033[31;1m'
white='\033[37;1m'
yellow='\033[33;1m'
clear
toilet -f slant "MINI-TESTING" -F metal
echo
echo $green"☜☆☞----------------------------☜☆☞"
echo $red"TOOL FOR INFO GATHERING OR PENTEST"
echo $green"☜☆☞----------------------------☜☆☞"
echo
echo $cyan"_______-------_______-------_______"
echo $cyan"_______-------_______-------_______"
echo
echo $red"ATHOUR --> MR.SPOON/2EUS666GH05T"
echo $red"TEAM --> MADURA DOWN SECURITY"
echo $red"VERSION --> TOOL VERSION 1.2"
echo $cyan"_______-------_______-------_______"
echo $cyan"_______-------_______-------_______"
read -p "ENTER WEBSITE TARGET:" tar;
clear

toilet -f slant "MINI-TESTING" -F metal
echo "=====================" 
echo "1.DNS LOOKUP"
echo "=====================" 
echo "2.WHOOIS LOOKUP" 
echo "====================="
echo "3.TRACEROUTE" 
echo "====================="
echo "4.REVERSE DNS LOOKUP"
echo "=====================" 
echo "5.GEOIP LOOKUP" 
echo "=====================" 
echo "6.PORT SCAN" 
echo "=====================" 
echo "7.REVERSE IP LOOKUP" 
echo "=====================" 
echo "8.NPING" 
echo "=====================" 
echo "9.HTTP HEADERS CHECK" 
echo "=====================" 
echo "10.ZONE TRANSFER"
echo "=====================" 
echo "11.SUBDOMAIN FINDER"
echo "=====================" 
echo "99.EXIT"
echo "=====================" 
echo $purple"MASUKAN PILIHAN ANDA"
read -p "|====>>>" bpc;


if [ $bpc = 1 ] || [ $bpc = 1 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl http://api.hackertarget.com/dnslookup/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi

if [ $bpc = 2 ] || [ $bpc = 2 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl http://api.hackertarget.com/whois/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 3 ] || [ $bpc = 3 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl https://api.hackertarget.com/mtr/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi

if [ $bpc = 4 ] || [ $bpc = 4 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl https://api.hackertarget.com/reversedns/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi

if [ $bpc = 5 ] || [ $bpc = 5 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl http://api.hackertarget.com/geoip/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi

if [ $bpc = 6 ] || [ $bpc = 6 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl http://api.hackertarget.com/nmap/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 7 ] || [ $bpc = 7 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
wget http://api.hackertarget.com/reverseiplookup/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 8 ] || [ $bpc = 8 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl https://api.hackertarget.com/nping/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 9 ] || [ $bpc = 9 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl https://api.hackertarget.com/httpheaders/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 10 ] || [ $bpc = 10 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl https://api.hackertarget.com/zonetransfer/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi

if [ $bpc = 11 ] || [ $bpc = 11 ]
then
clear
echo $green"scanning...."
echo $green"INFO -->"
sleep 1
echo $cyan"--------------------------------------------"
curl http://api.hackertarget.com/hostsearch/?q=$tar
echo
echo $cyan"--------------------------------------------"
fi


if [ $bpc = 99 ] || [ $bpc = 99 ]
then
clear
exit
fi